# DASC - Vorlesung Herbst 2023

* Vorlesungen und Übungen im Wahlpflichtmodul _"Data Science - from data to knowledge"_

* Dozent Prof. Dr. C. Schütze, Oktober - Dezember 2023

* Im Repository finden sich die Folien, Literaturhinweise, Beispielscripte in Python und R sowie die dazugehörigen Datensätze.
